//
//  SearchViewController.swift
//  Cinemind
//
//  Created by Pavel Iv on 11/19/16.
//  Copyright © 2016 Pavel Iv. All rights reserved.
//

import UIKit
import Parse

class SearchViewController: UITableViewController {

    let movieProvider = MovieProvider()
    var movieSearchResult = MovieSearchResult()
    var filteredMovies = [Movie]()
    var currentSearchQuery = ""
    
    var activityIndicator = UIActivityIndicatorView()
    let searchController = UISearchController(searchResultsController: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        activityIndicator.center = self.view.center
        tableView.addSubview(activityIndicator)
        
        startActivityIndicator()
        movieProvider.getPopularMovies(page: 1, completionHandler: searchResultRecived)
        
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
    
    func startActivityIndicator() {
        self.activityIndicator.startAnimating()
        self.activityIndicator.backgroundColor = UIColor.white
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
        self.activityIndicator.backgroundColor = UIColor.white
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        currentSearchQuery = searchText
        if currentSearchQuery == "" {
            movieProvider.getPopularMovies(page: 1, completionHandler: searchResultRecived)
        }
        
        filteredMovies = movieSearchResult.results.filter { movie in
           return movie.title.lowercased().contains(searchText.lowercased())
        }
        
        if filteredMovies.count == 0 {
            startActivityIndicator()
            movieProvider.findMovies(query: searchText, page: 1, completionHandler: searchResultRecived)
            return
        }
        
        tableView.reloadData()
    }
     
    func searchResultRecived(movieSearchResult: MovieSearchResult?){
        
        if (movieSearchResult!.page == 1) {
            self.movieSearchResult = movieSearchResult!
        }
        else {
            self.movieSearchResult.page = movieSearchResult!.page
            self.movieSearchResult.totalResults = movieSearchResult!.totalResults
            self.movieSearchResult.totalPages = movieSearchResult!.totalPages
            self.movieSearchResult.results.append(contentsOf: movieSearchResult!.results)
        }
        
        stopActivityIndicator()
        
        tableView.reloadData()
    }


    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numberOfRows = 0
        if searchController.isActive && searchController.searchBar.text != "" && filteredMovies.count > 0 {
            numberOfRows = filteredMovies.count
        }
        else {
            numberOfRows =  movieSearchResult.results.count
        }
        
        return numberOfRows
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "movieItemCell", for: indexPath) as! MovieItemCell
        let movie: Movie
        if searchController.isActive && searchController.searchBar.text != "" && filteredMovies.count > 0 {
            movie = filteredMovies[indexPath.row]
        }
        else {
            movie = movieSearchResult.results[indexPath.row]
        }
        
        cell.posterImg.downloadedFrom(link: movie.posterPath, contentMode: UIViewContentMode.scaleAspectFit)
        cell.title.text = movie.title
        cell.overview.text = movie.overview
        cell.releaseDate.text = movie.releaseDate
        cell.voteAvg.text = String(movie.voteAvg)
        
        checkForDownloadMore(rowIndex: indexPath.row)
        
        return cell
    }
    
    func checkForDownloadMore (rowIndex: Int) {
        if movieSearchResult.results.count < movieSearchResult.totalResults && (rowIndex == movieSearchResult.results.count - 1) {
            startActivityIndicator()
            
            if (currentSearchQuery == "") {
                movieProvider.getPopularMovies(page: movieSearchResult.page + 1, completionHandler: searchResultRecived)
            }
            else {
                movieProvider.findMovies(query: currentSearchQuery, page: movieSearchResult.page + 1, completionHandler: searchResultRecived)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            
            let movie = movieSearchResult.results[indexPath.row]
            let detailsVC = segue.destination as! MovieDetailsViewController
            detailsVC.title = movie.title
            detailsVC.movieId = movie.id
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SearchViewController: UISearchResultsUpdating{
    
    func updateSearchResults(for searchController: UISearchController){
        filterContentForSearchText(searchText: searchController.searchBar.text!)
    }
}
