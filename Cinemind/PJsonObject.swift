//
//  PJsonObject.swift
//  Cinemind
//
//  Created by Pavel Iv on 11/24/16.
//  Copyright © 2016 Pavel Iv. All rights reserved.
//

import UIKit

protocol PJsonObject {
    static func toObject(jsonStruct: [String:Any]) -> NSObject
}
