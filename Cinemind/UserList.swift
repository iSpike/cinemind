//
//  UserList.swift
//  Cinemind
//
//  Created by Pavel Iv on 11/25/16.
//  Copyright © 2016 Pavel Iv. All rights reserved.
//

import UIKit

class UserList: NSObject {
    var id: String = ""
    var name: String = ""
    var isDeletable: Bool = false
    var movieList = [Movie]()
}
