//
//  MovieProvider.swift
//  Cinemind
//
//  Created by Pavel Iv on 11/19/16.
//  Copyright © 2016 Pavel Iv. All rights reserved.
//

import UIKit

class MovieProvider: NSObject {
    
    func sendRequest<T: PJsonObject>(urlString: String, completionHandler: @escaping (_ data: T?) -> ()){
        let url = URL(string: urlString)
        URLSession.shared.dataTask(with:url!) { (data, response, error) in
            if error != nil {
                print(error)
            }
            else {
                do {
                    let parsedData = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [String:Any]
                    let result = T.toObject(jsonStruct: parsedData)
                    completionHandler(result as? T)
                }
                catch let error as NSError {
                    print(error)
                }
            }
        }.resume() 
    }
    
    func getPopularMovies(page: Int, completionHandler: @escaping (_ data: MovieSearchResult?) -> ()){
        
        let urlString = "\(AppConstants.tmdbMovieBaseUrl)/movie/popular?language=en-US&api_key=\(AppConstants.tmdbApiKey)&page=\(page)"
        
        sendRequest(urlString: urlString, completionHandler: completionHandler)
    }
    
    func findMovies(query: String, page: Int, completionHandler: @escaping (_ data: MovieSearchResult?) -> ()){
        let escapedString = query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        let urlString = "\(AppConstants.tmdbMovieBaseUrl)/search/movie?language=en-US&api_key=\(AppConstants.tmdbApiKey)&page=\(page)&query=\(escapedString!)"
        
        sendRequest(urlString: urlString, completionHandler: completionHandler)
    }
    
    func getMovieDetails(movieId: Int, completionHandler: @escaping (_ data: MovieDetails?) -> ()){
        
        let urlString = "\(AppConstants.tmdbMovieBaseUrl)/movie/\(movieId)?api_key=\(AppConstants.tmdbApiKey)"
        
        sendRequest(urlString: urlString, completionHandler: completionHandler)
    }
}
