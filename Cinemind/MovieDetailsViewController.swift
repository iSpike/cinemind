//
//  MovieDetailsViewController.swift
//  Cinemind
//
//  Created by Pavel Iv on 11/23/16.
//  Copyright © 2016 Pavel Iv. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UITableViewController {

    let movieProvider = MovieProvider()
    var movieId = 0
    var movieDetails = MovieDetails()
    
    @IBOutlet weak var posterImg: UIImageView!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var geners: UILabel!
    @IBOutlet weak var homepage: UILabel!
    //@IBOutlet weak var movieTitle: UINavigationItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        movieProvider.getMovieDetails(movieId: movieId, completionHandler: detailsRecived)
    }

    
    func detailsRecived(movieDetails: MovieDetails?){
        if let details = movieDetails {
            self.movieDetails = details
    
            posterImg.downloadedFrom(link: self.movieDetails.posterPath, contentMode: UIViewContentMode.scaleAspectFit)
            
            overview.text = self.movieDetails.overview
            geners.text = "Geners: \(self.movieDetails.genres.joined(separator: ", "))"
            homepage.text = self.movieDetails.homepage
            
            tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
