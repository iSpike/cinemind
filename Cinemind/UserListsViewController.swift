//
//  UserListsViewController.swift
//  Cinemind
//
//  Created by Pavel Iv on 11/19/16.
//  Copyright © 2016 Pavel Iv. All rights reserved.
//

import UIKit
import Parse

class UserListsViewController: UITableViewController {

    var userLists = [UserList]()
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let query = PFQuery(className:"UserList")
        query.whereKey("user", equalTo:PFUser.current()!)
        query.findObjectsInBackground(block: { (objects, error) in
     
            if let listObjects = objects{
                listObjects.forEach({ (record) in
                    let list = UserList()
                    list.id = record.objectId!
                    list.name = record["listName"] as! String
                    list.isDeletable = record["isDeletable"] as! Bool
                    list.movieList = [Movie]()
                    let subQuery = PFQuery(className:"Movie")
                    subQuery.whereKey("userList", equalTo: record)
                    subQuery.findObjectsInBackground(block: { (objects, error) in
                        if let movieObjects = objects{
                            movieObjects.forEach({ (record) in
                                let movie = Movie()
                                movie.id = record["movieId"] as! Int
                                movie.overview = record["overview"] as! String
                                movie.posterPath = record["posterPath"] as! String
                                movie.releaseDate = record["releaseDate"] as! String
                                movie.title = record["title"] as! String
                                movie.voteAvg = record["voteAvg"] as! Double
                                movie.title = record["title"] as! String
                                list.movieList.append(movie)
                            })
                        }
                    })
                  
                    self.userLists.append(list)
                })
                
                self.tableView.reloadData()
            }
        })

    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return userLists.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listItemCell", for: indexPath)

        cell.textLabel?.text = userLists[indexPath.row].name

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let indexPath = tableView.indexPathForSelectedRow {
            
            let list: UserList = userLists[indexPath.row]
            let listMoviesVC = segue.destination as! ListMoviesViewController
            listMoviesVC.list = list
            
        }
    }
 

}
