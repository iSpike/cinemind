//
//  MovieDetails.swift
//  Cinemind
//
//  Created by Pavel Iv on 11/23/16.
//  Copyright © 2016 Pavel Iv. All rights reserved.
//

import UIKit

class MovieDetails: NSObject, PJsonObject {
    var id: Int = 0
    var posterPath: String = ""
    var title: String = ""
    var overview: String = ""
    var releaseDate: String = ""
    var voteAvg: Double = 0.0
    var runtime: NSDate = NSDate()
    var homepage: String = ""
    var genres: [String] = [String]()
    
    static func toObject(jsonStruct: [String:Any]) -> NSObject {
        let object = MovieDetails()
        
        if let idVal = jsonStruct["id"] as? Int {
            object.id = idVal
        }
        
        if let posterPathVal = jsonStruct["poster_path"] as? String {
            object.posterPath = "\(AppConstants.tmdbImageBaseUrl)/w154\(posterPathVal)"
        }
        
        if let titleVal = jsonStruct["title"] as? String {
            object.title = titleVal
        }
        
        if let overviewVal = jsonStruct["overview"] as? String {
            object.overview = overviewVal
        }
        
        if let releaseDateVal = jsonStruct["release_date"] as? String {
            object.releaseDate = releaseDateVal
        }
        
        if let voteAverageVal = jsonStruct["vote_average"] as? Double {
            object.voteAvg = voteAverageVal
        }
        
        if let homepageVal = jsonStruct["homepage"] as? String {
            object.homepage = homepageVal
        }
        
        //if let runtime = jsonStruct["runtime"] as? Int {
        //    object.voteAvg = jsonStruct["vote_average"] as! Double
        //}
        
        if let genresList = jsonStruct["genres"] as? [Any]  {
            
            genresList.forEach({ (item: Any) in
                if let itemContent = item as? [String:Any] {
                    object.genres.append(itemContent["name"] as! String)
                }
            })
        }
        
        return object
    }
}
 
 
