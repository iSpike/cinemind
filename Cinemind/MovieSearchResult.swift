//
//  movieSearchResult.swift
//  Cinemind
//
//  Created by Pavel Iv on 11/19/16.
//  Copyright © 2016 Pavel Iv. All rights reserved.
//

import UIKit

class MovieSearchResult: NSObject, PJsonObject {
    var totalResults: Int = 0
    var totalPages: Int = 0
    var page: Int = 0
    var results = [Movie]()
    
    static func toObject(jsonStruct: [String:Any]) -> NSObject {
        let object = MovieSearchResult()
        
        if let totalResultsVal = jsonStruct["total_results"] as? Int {
            object.totalResults = totalResultsVal
        }
        
        if let totalPagesVal = jsonStruct["total_pages"] as? Int {
            object.totalPages = totalPagesVal
        }
        
        if let pageVal = jsonStruct["page"] as? Int {
            object.page = pageVal
        }
        
        if let resultsVal = jsonStruct["results"] as? [[String:Any]] {
            resultsVal.forEach({ (item: [String : Any]) in
                object.results.append(Movie.toObject(jsonStruct: item) as! Movie)
            })
        }
        
        return object
    }
}


