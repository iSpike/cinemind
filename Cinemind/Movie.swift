//
//  UserListMovie.swift
//  Cinemind
//
//  Created by Pavel Iv on 11/25/16.
//  Copyright © 2016 Pavel Iv. All rights reserved.
//

import UIKit

class Movie: NSObject, PJsonObject {
    var id: Int = 0
    var posterPath: String = ""
    var title: String = ""
    var overview: String = ""
    var releaseDate: String = ""
    var voteAvg: Double = 0.0
    
    static func toObject(jsonStruct: [String:Any]) -> NSObject {
        let object = Movie()
        
        if let idVal = jsonStruct["id"] as? Int {
            object.id = idVal
        }
        
        if let posterPathVal = jsonStruct["poster_path"] as? String {
            object.posterPath = "\(AppConstants.tmdbImageBaseUrl)/w154\(posterPathVal)"
        }
        
        if let titleVal = jsonStruct["title"] as? String {
            object.title = titleVal
        }
        
        if let overviewVal = jsonStruct["overview"] as? String {
            object.overview = overviewVal
        }
        
        if let releaseDateVal = jsonStruct["release_date"] as? String {
            object.releaseDate = releaseDateVal
        }
        
        if let voteAverageVal = jsonStruct["vote_average"] as? Double {
            object.voteAvg = voteAverageVal
        }
        
        return object
    }
}
