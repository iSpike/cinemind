//
//  AppConstants.swift
//  Cinemind
//
//  Created by Pavel Iv on 11/19/16.
//  Copyright © 2016 Pavel Iv. All rights reserved.
//

import UIKit

class AppConstants: NSObject {
    //////////////////The Movie DB Api/////////////////////////
    static let tmdbApiKey = "86239335363ceb5009b45366ea4eb8ed"
    
    static let tmdbMovieBaseUrl = "https://api.themoviedb.org/3"
    
    static let tmdbImageBaseUrl = "https://image.tmdb.org/t/p"
    
    //////////////////Parse Api////////////////////////////////
    
    static let parseAppId = "6DREIOta8iIBe7lEFiM4hAe4KhrReC36Zhy36GbJ"
    
    static let parseClientKey = "sFwVzZfvHxxxEP9QnruJ60XRdIGDxQAvdpEvXxG6"
    
    static let parseServer = "https://parseapi.back4app.com/"
}
